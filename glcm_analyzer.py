import json
import matplotlib.pyplot as plt
from DataObject import DataObject

with open('data.json') as fp:
    labeled_data = json.load(fp)

forest_data = labeled_data['forest'][0]
sea_data = labeled_data['sea'][0]
snow_data = labeled_data['snow'][0]
mountain_data = labeled_data['mountain'][0]

forest = DataObject(forest_data)
forest.show_sample_glcm()
plt.show()