import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os
import json
from copy import deepcopy


DATASET_DIR = './dataset/'
points_per_image = 5
images_to_analyse = 2

classes = next(os.walk(DATASET_DIR))[1]

coords_template = {"path": "", "points": []}
coords = {c: [] for c in classes}
current_class = classes[0]


def onclick(event):
    global ix, iy
    ix, iy = event.xdata, event.ydata
    print(f'x = {ix}, y = {iy}')

    plt.scatter(ix, iy)
    plt.draw()

    global coords
    global current_class
    coords[current_class][-1]["points"].append({"x": int(ix), "y": int(iy)})

    if len(coords[current_class][-1]["points"]) == points_per_image:
        fig.canvas.mpl_disconnect(cid)
        plt.close()

    return coords


for c in classes:
    current_class = c
    for i in range(images_to_analyse):
        coords[current_class].append(deepcopy(coords_template))
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cid = fig.canvas.mpl_connect('button_press_event', onclick)

        img_dir = os.path.join(DATASET_DIR, c)
        img_file = next(os.walk(img_dir))[2][i]  # get the first img for class
        img_path = os.path.join(img_dir, img_file)
        coords[current_class][-1]['path'] = img_path
        img = mpimg.imread(img_path)

        ax.imshow(img)
        plt.title(f"Label {c}")
        plt.show()

with open('data.json', 'w') as fp:
    json.dump(coords, fp, sort_keys=True, indent=4)
