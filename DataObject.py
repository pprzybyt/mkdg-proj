import cv2
from skimage.feature import greycomatrix, greycoprops
import matplotlib.pyplot as plt

PATCH_SIZE = 21


class DataObject:
    def __init__(self, category_json):
        self.category_json = category_json
        self.gray_image = self.load_image()
        self.points = []
        self.patches = []
        self.load_points()
        self.load_patches()
        self.glcm_properties = {
            'dissimilarity': [],
            'correlation': [],
            'homogeneity': [],
            'ASM': [],
            'energy': []
        }
        self.load_glcm_properties()

    def load_image(self):
        image = cv2.imread(self.category_json['path'])
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return gray_image

    def load_points(self):
        for point in self.category_json['points']:
            self.points.append((point['y'], point['x']))

    def load_patches(self):
        for loc in self.points:
            self.patches.append(self.gray_image[loc[0]:loc[0] + PATCH_SIZE, loc[1]:loc[1] + PATCH_SIZE])

    def load_glcm_properties(self):
        for patch in self.patches:
            glcm = greycomatrix(patch, [5], [0], 256, symmetric=True, normed=True)
            self.glcm_properties['dissimilarity'].append(greycoprops(glcm, 'dissimilarity')[0, 0])
            self.glcm_properties['correlation'].append(greycoprops(glcm, 'correlation')[0, 0])
            self.glcm_properties['homogeneity'].append(greycoprops(glcm, 'homogeneity')[0, 0])
            self.glcm_properties['ASM'].append(greycoprops(glcm, 'ASM')[0, 0])
            self.glcm_properties['energy'].append(greycoprops(glcm, 'energy')[0, 0])

    def show_sample_glcm(self):
        # create the figure
        fig = plt.figure(figsize=(10, 6))

        # display original image with locations of patches
        ax = fig.add_subplot(2, 2, 1)
        ax.imshow(self.gray_image, cmap=plt.cm.gray, interpolation='nearest',
                  vmin=0, vmax=255)
        for (y, x) in self.points:
            ax.plot(x + PATCH_SIZE / 2, y + PATCH_SIZE / 2, 'gs')
        ax.set_xlabel('Original Image')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.axis('image')

        # for each patch, plot (dissimilarity, correlation)
        ax = fig.add_subplot(2, 2, 2)
        ax.plot(self.glcm_properties['dissimilarity'][:len(self.patches)],
                self.glcm_properties['correlation'][:len(self.patches)], 'go')
        ax.set_xlabel('GLCM Dissimilarity')
        ax.set_ylabel('GLCM Correlation')

        # display the image patches
        for i, patch in enumerate(self.patches):
            ax = fig.add_subplot(2, len(self.patches), len(self.patches)*1 + i + 1)
            ax.imshow(patch, cmap=plt.cm.gray, interpolation='nearest',
                      vmin=0, vmax=255)
            ax.set_xlabel('Patch %d' % (i + 1))
